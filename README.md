# Network Manager Configuration

## System Default Connections

To install default NetworkManager system connections, put your
network manager connection files in /pvconfigs/nm-connections/ folder.

On startup we will seed those into /etc/NetworkManager/system-connections
overwriting files that might already exist there.

## System Firewalls

In order to load firewall rules from "iptables" files saved by iptables-restore
simply put those the _config/etc/iptables/rules.v4

THis platform will load those rules on startup

## dbus proxying

Sometimes is useful to make platforms share the same dbus. This platform
offers the ability to create per id proxies of dbus that then can be
offered to platforms as needed.

To setup a dbus proxy you would use files/usr/share/docs/debian-nm/examples/pvdbus-proxy.target
and install it into /etc/systemd/system/ folder.

The example shows how to create proxies for uid 1000 and 0, e.g.

```
Requires=pvdbus-proxy@0.service pvdbus-proxy@1000.service
```

You can change that line accordingly.

The systembus will be availalbe in /pvrun/dbus/UID/ directory, e.g.

```
/pvrun/dbus/1000/system_bus_socket
```

